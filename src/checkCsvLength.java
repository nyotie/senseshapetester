import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Stream;

/**
 * Created by nyotie on 19-Jun-17.
 */
public class checkCsvLength {
    static String testingDatasetPath = "";
    static String filterDataPath = "./data/folderB";
    static int minimumRc = 40 + 1; // +1 for header

    public static void main(String[] args) throws Exception {
//--------------------------------------------------------- log
        Logger logger = Logger.getLogger("data log");
        FileHandler fh;
        fh = new FileHandler("./data/log.txt");
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        StringBuilder logMesg = new StringBuilder("");
        logMesg.append(System.lineSeparator());
        logMesg.append("File info: " + System.lineSeparator());

//--------------------------------------------------------- files
        ArrayList<String> listOfFiles = new ArrayList<>();
        File isolationRoom = new File(filterDataPath);
        try(Stream<Path> paths = Files.walk(Paths.get(testingDatasetPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

//--------------------------------------------------------- process
        int fileCount = 0;
        int minRow = 0, totalRow = 0, totalCol = 0;
        int rowB = 0, rowR = 0, rowS = 0, rowT = 0, rowO = 0;
        int labelB = 0, labelR = 0, labelS = 0, labelT = 0, labelO = 0;
        String leastRowFile = "";
        for (String thisFilePath: listOfFiles){
            File rawSource = new File(thisFilePath);
            CSVReader reader = new CSVReader(new FileReader(rawSource));

            List<String[]> csvData = reader.readAll();
            reader.close();

            totalRow += csvData.size();

            String[] thisCsvInfo = getSegmentLabel(csvData);
            totalCol = Integer.valueOf(thisCsvInfo[0]);

            logMesg.append(rawSource.getName() + " --> label:" + thisCsvInfo[1] + ", row:" + csvData.size() + ", col:" + totalCol);
            if(csvData.size() < minimumRc){
                logMesg.append(rawSource.getName() + " has row count: " + csvData.size());
                File newPlace = new File(isolationRoom.getPath() + "/" + rawSource.getName());
                rawSource.renameTo(newPlace);
            }
            if(totalCol==0)
                totalCol = Integer.valueOf(thisCsvInfo[0]);
            else if(totalCol != Integer.valueOf(thisCsvInfo[0])){
                totalCol = Integer.valueOf(thisCsvInfo[0]);
                logMesg.append(". Warning! have different column count number");
            }
            logMesg.append(System.lineSeparator());

            switch (thisCsvInfo[1]){
                case "B":
                    labelB++;
                    rowB += csvData.size();
                    break;
                case "R":
                    labelR++;
                    rowR += csvData.size();
                    break;
                case "S":
                    labelS++;
                    rowS += csvData.size();
                    break;
                case "T":
                    labelT++;
                    rowT += csvData.size();
                    break;
                case "O":
                    labelO++;
                    rowO += csvData.size();
                    break;
            }

            if (minRow==0 || (csvData.size()<minRow && csvData.size() > minimumRc)) {
                leastRowFile = rawSource.getName();
                minRow = csvData.size();
            }

            fileCount++;
        }

        logMesg.append(System.lineSeparator());
        logMesg.append("Total File: " + fileCount + ", total row: " + totalRow + System.lineSeparator());
        logMesg.append("B: " + labelB + ", row: " + rowB + System.lineSeparator());
        logMesg.append("R: " + labelR + ", row: " + rowR + System.lineSeparator());
        logMesg.append("S: " + labelS + ", row: " + rowS + System.lineSeparator());
        logMesg.append("T: " + labelT + ", row: " + rowT + System.lineSeparator());
        logMesg.append("O: " + labelO + ", row: " + rowO + System.lineSeparator());
        logMesg.append(System.lineSeparator());
        logMesg.append("minimum row: " + leastRowFile +  " --> " + minRow + System.lineSeparator());
        logMesg.append(System.lineSeparator());
        logMesg.append("column count: " + totalCol + System.lineSeparator());

        logger.info(logMesg.toString());
    }

    private static String[] getSegmentLabel(List<String[]> thisIsShit){
        String[] label = new String[2];

        boolean first = true;
        for (String[] lineTokens : thisIsShit) {
            if(first) {
                first=false;
                label[0] = String.valueOf(lineTokens.length-1);
            } else {
                label[1] = lineTokens[Integer.valueOf(label[0])];
                break;
            }
        }

        return label;
    }
}
