import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by nyotie on 08-Aug-17.
 */
public class ParseTreeToIfElse {
    static String ModelTree = "./data/tree_feature_30.txt";

    public static void main(String[] args) {
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(ModelTree);
            br = new BufferedReader(fr);

            String sCurrentLine;
            for (int i = 0; i<=2; i++){ // just to dump 3 first rows
                br.readLine();
            }

            while ((sCurrentLine = br.readLine()) != null) {
                sCurrentLine = sCurrentLine.trim().replace("|","").replace(" ",""); // trim spaces and remove vertical lines

                if (sCurrentLine.length() < 1) break; // must be one line before number of leaves is written

                boolean opener = sCurrentLine.contains("<=");
                boolean haveClass = sCurrentLine.contains(":");

                String ShapeletID, ShapeletValue, ShapeletLabel;
                ShapeletID = sCurrentLine.substring(0,sCurrentLine.indexOf((opener)? "<" : ">"));
                ShapeletValue = sCurrentLine.substring(sCurrentLine.indexOf((opener)? "<" : ">"), (haveClass) ? sCurrentLine.indexOf(":") : sCurrentLine.length());
                ShapeletLabel = (haveClass) ? sCurrentLine.substring(sCurrentLine.indexOf(":")+1, sCurrentLine.indexOf(":")+2) : "";

                if (opener){ // contains <=
                    System.out.print("if (CalculateSimilarity(\""+ ShapeletID +"\")"+ ShapeletValue +")");
                    if (haveClass)
                        System.out.println(" result = \"" + ShapeletLabel + "\";");
                    else
                        System.out.println();

                } else { // contains >
                    System.out.print("else");
                    if (haveClass)
                        System.out.println(" result = \"" + ShapeletLabel + "\";");
                    else
                        System.out.println();
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
