import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.invoke.SwitchPoint;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by nyotie on 29-Jul-17.
 */
public class RecheckResult {
    static String testingDatasetPath = "./data/folderB";

    public static void main(String[] args) {
        ArrayList<String> listOfFiles = new ArrayList<>();
        try(Stream<Path> paths = Files.walk(Paths.get(testingDatasetPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String thisFilePath: listOfFiles){
            File rawSource = new File(thisFilePath);
            System.out.println(rawSource.getName());
            RunTheShitPlease(thisFilePath);
            System.out.println();
        }
    }

    private static void RunTheShitPlease(String location){
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(location);
            br = new BufferedReader(fr);

            String sCurrentLine;

            double realR=0, realB=0, realO=0;
            double correctR=0, correctB=0, correctO=0;
            double predictedR=0, predictedB=0, predictedO=0;
            double totalPrediction=0, totalCorrectPrediction=0;

            boolean startParsing = false;
            while ((sCurrentLine = br.readLine()) != null) {
                if(sCurrentLine.contains(".csv:")) {
                    startParsing = true;
                } else if(sCurrentLine.contains("Correct Prediction")) break;

                if(!startParsing) continue;

                String groundTruth, prediction;
                groundTruth = sCurrentLine.substring(sCurrentLine.length() - 4, sCurrentLine.length() - 3);
                prediction = sCurrentLine.substring(sCurrentLine.length() - 1, sCurrentLine.length());

                switch (groundTruth){
                    case "R": realR++;
                        break;
                    case "B": realB++;
                        break;
                    case "O": realO++;
                        break;
                }

                switch (prediction){
                    case "R": predictedR++;
                        break;
                    case "B": predictedB++;
                        break;
                    case "O": predictedO++;
                        break;
                }

                if(Objects.equals(groundTruth, prediction)){
                    totalCorrectPrediction++;

                    switch (groundTruth){
                        case "R": correctR++;
                            break;
                        case "B": correctB++;
                            break;
                        case "O": correctO++;
                            break;
                    }
                }

//                System.out.println("gt=" + groundTruth + ", p=" + prediction);
                totalPrediction++;
            }

            // precission = true positive / total prediction
            // recall = true positive / total actual class

            double precisionR = (predictedR==0) ? 0 : correctR / predictedR;
            double precisionB = (predictedB==0) ? 0 : correctB / predictedB;
            double precisionO = (predictedO==0) ? 0 : correctO / predictedO;

            double recallR = correctR / realR;
            double recallB = correctB / realB;
            double recallO = correctO / realO;

            double accuracy = totalCorrectPrediction/totalPrediction;

//            vertical style
//            System.out.println("precision R:" + correctR +"/"+ predictedR + "->" + precisionR*100);
//            System.out.println("precision B:" + correctB +"/"+ predictedB + "->" + precisionB*100);
//            System.out.println("precision O:" + correctO +"/"+ predictedO + "->" + precisionO*100);
//            System.out.println("recall R:" + correctR +"/"+ realR + "->" + recallR*100);
//            System.out.println("recall B:" + correctB +"/"+ realB + "->" + recallB*100);
//            System.out.println("recall O:" + correctO +"/"+ realO + "->" + recallO*100);
//            System.out.println(totalCorrectPrediction + "/" + totalPrediction + "=" + accuracy*100);

//            tab style
            System.out.println("PrecR\tPrecB\tPrecO\tRecR\tRecB\tRecO\tAccuracy");
            System.out.println(precisionR+"\t"+precisionB+"\t"+precisionO+"\t"+recallR+"\t"+recallB+"\t"+recallO+"\t"+accuracy);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
