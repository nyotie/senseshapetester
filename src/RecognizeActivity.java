import Features.ExtractStatisticFeatures;
import Filter.*;
import distance.*;
import Segment.*;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class RecognizeActivity {

    // paths
    static String testingDatasetPath = "./data/testData";
    static String filteredDataPath = "./data/filteredData";

    // Config: Shapelet Path
    static String shapeletPath = "./data/shape_feature_30.csv";

    // Config: Similarity Calculation
    static int distanceMeasurement = 1; // 0=DTW, 1=ED, 2=PCC

    // Config: Smoothing Filter
    private static boolean filterFirst = true;
    static int filterMethod = 1; // 0=Mean, 1=Median, 2=LPF
    static int filterWindowSize = 10;

    // FINAL parameters for Confusion Matrix
    static double realR=0, realB=0, realO=0;
    static double correctR=0, correctB=0, correctO=0;
    static double predictedR=0, predictedB=0, predictedO=0;
    static double totalPrediction=0, totalCorrectPrediction=0;

    public static void main(String[] args) throws Exception {
        Instant start = Instant.now();

        // todo: read the shapeletData
        CSVReader readShapelets = new CSVReader(new FileReader(shapeletPath));
        String[] shapeletHeader = readShapelets.readNext();
        ArrayList<float[]> shapeletData = rowToColumn_Float(readShapelets.readAll(), shapeletHeader.length);

        // todo: prepare classifier
        ShapeletClassifier classifier = new ShapeletClassifier(distanceMeasurement, shapeletHeader, shapeletData);

        ArrayList<String> listOfFiles = new ArrayList<>();
        // todo: list down the unlabelled instances
        try(Stream<Path> paths = Files.walk(Paths.get(testingDatasetPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // todo: apply smoothing filter or not to the unlabelled instances
        if(filterFirst) filterTimeSeries(listOfFiles);

        for (String thisFilePath: listOfFiles) {
            File thisFile = new File(thisFilePath);

            CSVReader readInstance = new CSVReader(new FileReader(thisFile)); // read template csv and store to variable
            int colCount = readInstance.readNext().length - 1;
            List<String[]> victimValues = readInstance.readAll();

            ArrayList<float[]> listSegment = rowToColumn_Float(victimValues, colCount);
            classifier.setUnlabelledData(listSegment);

            // first branch of checking
            System.out.print(thisFile.getName() + ": ");
            String groundTruth = getSegmentLabel(victimValues, colCount);
            String result;

            result = classifier.Classify();

            System.out.print(groundTruth + "->");
            System.out.println(result);

            switch (groundTruth){
                case "R": realR++;
                    break;
                case "B": realB++;
                    break;
                case "O": realO++;
                    break;
            }

            switch (result){
                case "R": predictedR++;
                    break;
                case "B": predictedB++;
                    break;
                case "O": predictedO++;
                    break;
            }

            if(Objects.equals(groundTruth, result)){
                totalCorrectPrediction++;

                switch (groundTruth){
                    case "R": correctR++;
                        break;
                    case "B": correctB++;
                        break;
                    case "O": correctO++;
                        break;
                }
            }

            totalPrediction += 1;
        }

        double precisionR = (predictedR==0) ? 0 : correctR / predictedR;
        double precisionB = (predictedB==0) ? 0 : correctB / predictedB;
        double precisionO = (predictedO==0) ? 0 : correctO / predictedO;

        double recallR = correctR / realR;
        double recallB = correctB / realB;
        double recallO = correctO / realO;

        double accuracy = totalCorrectPrediction / totalPrediction;

//      vertical style
        System.out.println(); System.out.println("--------------------------------");
        System.out.println("precision R: " + correctR +"/"+ predictedR + " -> " + precisionR*100);
        System.out.println("precision B: " + correctB +"/"+ predictedB + " -> " + precisionB*100);
        System.out.println("precision O: " + correctO +"/"+ predictedO + " -> " + precisionO*100);
        System.out.println();
        System.out.println("recall R: " + correctR +"/"+ realR + " -> " + recallR*100);
        System.out.println("recall B: " + correctB +"/"+ realB + " -> " + recallB*100);
        System.out.println("recall O: " + correctO +"/"+ realO + " -> " + recallO*100);
        System.out.println();
        System.out.println("Accuracy: " + totalCorrectPrediction + "/" + totalPrediction + " = " + accuracy*100);
        System.out.println("--------------------------------"); System.out.println();

//        tab style --> USEFUL to copy paste to excel
//        System.out.println("PrecR\tPrecB\tPrecO\tRecR\tRecB\tRecO\tAccuracy");
//        System.out.println(precisionR+"\t"+precisionB+"\t"+precisionO+"\t"+recallR+"\t"+recallB+"\t"+recallO+"\t"+accuracy);

        Instant stop = Instant.now();
        Duration d = Duration.between( start , stop ) ;
        long minutesPart = d.toMinutes();
        long secondsPart = d.minusMinutes( minutesPart ).getSeconds() ;

        System.out.println( "Interval: " + start + " - " + stop );
        System.out.println( "Elapsed: " + minutesPart + "M " + secondsPart + "S" );
    }

    private static ArrayList<float[]> rowToColumn_Float(List<String[]> thisIsShit, int colCount){
        ArrayList<float[]> notShit = new ArrayList<>();

        for(int i = 0; i <= colCount-1; ++i){ //iterate column, not including the last column (is a string for label)
            float[] tempDoubleArrayList = new float[thisIsShit.size()];

            int rowCount = 0;

            for (String[] lineTokens : thisIsShit) {
                if (lineTokens[i].trim().length() == 0){
                    float[] changeArraySize = Arrays.copyOfRange(tempDoubleArrayList, 0,  rowCount);
                    tempDoubleArrayList = new float[rowCount];
                    System.arraycopy(changeArraySize, 0, tempDoubleArrayList, 0, rowCount);

                    break;
                }

                tempDoubleArrayList[rowCount] = Float.valueOf(lineTokens[i]);
                rowCount++;
            }
            notShit.add(tempDoubleArrayList);
        }

        return notShit;
    }

    private static String getSegmentLabel(List<String[]> thisIsShit, int colCount){
        String label = "";
        for (String[] lineTokens : thisIsShit) {
            label = lineTokens[colCount];
            break;
        }

        return label;
    }

    private static void filterTimeSeries(ArrayList<String> listOfFiles) throws IOException {
        System.out.println("Filter the raw data to smooth it.");

        // prepare data directory to store filtered csv files
        if(new File(filteredDataPath).exists()){
            for(File file: new File(filteredDataPath).listFiles())
                if (!file.isDirectory()) file.delete();
        } else {
            new File(filteredDataPath).mkdir();
        }

        //todo: read all csv then prep var sets for next step
        for (String thisFilePath: listOfFiles){
            File rawSource = new File(thisFilePath);
            String writePath = filteredDataPath + "/F_" + rawSource.getName();
            CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, new File(writePath).exists()));

            //todo: read data from csv file
            CSVReader reader = new CSVReader(new FileReader(rawSource)); // read template csv and store to variable
            String[] headerName = reader.readNext();
            csvWriter.writeNext(headerName);
            int colCount = headerName.length;

            // variable used
            String[] nextLine;
            String thisLineValue="";

            //todo: apply filter
            if(filterMethod==0 || filterMethod==1){
                MeanMedian mmAccX = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmAccY = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmAccZ = new MeanMedian(filterWindowSize, filterMethod);

                MeanMedian mmGyroX = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmGyroY = new MeanMedian(filterWindowSize, filterMethod);
                MeanMedian mmGyroZ = new MeanMedian(filterWindowSize, filterMethod);

                while ((nextLine = reader.readNext()) != null) {
                    thisLineValue += mmAccX.updateSample(Float.valueOf(nextLine[0])) + ",";
                    thisLineValue += mmAccY.updateSample(Float.valueOf(nextLine[1])) + ",";
                    thisLineValue += mmAccZ.updateSample(Float.valueOf(nextLine[2])) + ",";
                    thisLineValue += mmGyroX.updateSample(Float.valueOf(nextLine[3])) + ",";
                    thisLineValue += mmGyroY.updateSample(Float.valueOf(nextLine[4])) + ",";
                    thisLineValue += mmGyroZ.updateSample(Float.valueOf(nextLine[5])) + ",";

                    for (int i=6; i<=colCount-1; i++){
                        thisLineValue += nextLine[i] + ((i!=colCount-1) ? "," : "");
                    }

                    csvWriter.writeNext(thisLineValue.split(","));
                    thisLineValue="";
                }
            } else {
                float[] filteredAcc = new float[3];

                while ((nextLine = reader.readNext()) != null) {
                    filteredAcc = LowPass.filter(Arrays.copyOfRange(nextLine, 0, 3) ,filteredAcc);
                    thisLineValue = filteredAcc[0] + "," + filteredAcc[1] + "," + filteredAcc[2] + ",";
                    for (int i=3; i<=colCount-1; i++){
                        thisLineValue += nextLine[i] + ((i!=colCount-1) ? "," : "");
                    }

                    csvWriter.writeNext(thisLineValue.split(","));
                    thisLineValue="";
                }
            }
            csvWriter.close();
        }

        listOfFiles.clear();
        try(Stream<Path> paths = Files.walk(Paths.get(filteredDataPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
