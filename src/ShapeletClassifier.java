import Features.ExtractStatisticFeatures;
import Segment.timeSeriesSegmentation;
import distance.DTW;
import distance.EuclideanDistance;
import distance.PearsonCorrelation;

import java.util.ArrayList;

/**
 * Created by nyotie on 08-Aug-17.
 */
public class ShapeletClassifier {
    private ArrayList<float[]> unlabelledData, shapeletData;
    private String[] ShapeletHeader;
    private int distanceMeasurement;

    public String Classify(){
        String result;

        // start of Classification Tree Model
        if (CalculateSimilarity("434_11_30_5_5_Med")<=-0.317375)
            if (CalculateSimilarity("140_2_30_5_5_Mean")<=-0.091256)
                result = "R";
            else
                if (CalculateSimilarity("40_2_30_5_1_MCR")<=-1)
                    if (CalculateSimilarity("4_2_30_5_4_ZCR")<=-1)
                        result = "B";
                    else
                        result = "R";
                else
                    result = "B";
        else
            if (CalculateSimilarity("57_11_30_5_7_RMS")<=-2.171265)
                result = "O";
            else
                if (CalculateSimilarity("57_11_30_5_7_RMS")<=0.097324)
                    result = "R";
                else
                    if (CalculateSimilarity("4_2_30_5_4_Mean")<=0.115499)
                        result = "R";
                    else
                        result = "B";
        // end of Classification Tree Model

        return result;
    }


    // --------------------------------------------------------------------------- CALLER
    public ShapeletClassifier(){
        super();
    }

    public ShapeletClassifier(int distanceMeasurement, String[] shapeletHeader, ArrayList<float[]> shapeletData){
        setDistanceMeasurement(distanceMeasurement);
        setShapeletHeader(shapeletHeader);
        setShapeletData(shapeletData);
    }

    // --------------------------------------------------------------------------- SETTER AND GETTER
    public void setDistanceMeasurement(int distanceMeasurement) {
        this.distanceMeasurement = distanceMeasurement;
    }

    public void setShapeletData(ArrayList<float[]> shapeletData) {
        this.shapeletData = shapeletData;
    }

    public void setShapeletHeader(String[] shapeletHeader) {
        ShapeletHeader = shapeletHeader;
    }

    public void setUnlabelledData(ArrayList<float[]> unlabelledData) {
        this.unlabelledData = unlabelledData;
    }

    private int getShapeletIndex(String[] ShapeletHeader, String ShapeletId){
        int idx = 0;
        for(String shapelet: ShapeletHeader){
            if(shapelet.equals(ShapeletId)) break; else idx++;
        }

        return idx;
    }

    // --------------------------------------------------------------------------- FUNCTIONS
    public double CalculateSimilarity(String ShapeletId){
        String[] Info = ShapeletId.split("_");
        int ShapeletIndex = getShapeletIndex(ShapeletHeader, ShapeletId);

        double leastDistance = 0;
        float[] leastDistanceSegment = new float[0];

        // segment one dimension of time series from unlabelled instance, according to column id in shapelet
        ArrayList<float[]> segmentOfColumnX =  timeSeriesSegmentation.slideWindow(Integer.valueOf(Info[2]), 1, unlabelledData.get(Integer.valueOf(Info[1])));

        for(float[] thisSegment: segmentOfColumnX) { // find best-matching-location
            double tempDistance;
            if (distanceMeasurement == 0) {
                DTW calcDTW = new DTW(thisSegment, shapeletData.get(ShapeletIndex));
                tempDistance = calcDTW.getWarpingDistance();
            } else if (distanceMeasurement == 1) {
                EuclideanDistance calcED = new EuclideanDistance(thisSegment, shapeletData.get(ShapeletIndex));
                tempDistance = calcED.getTotalDistance();
            } else {
                PearsonCorrelation calcPCC = new PearsonCorrelation(thisSegment, shapeletData.get(ShapeletIndex));
                tempDistance = calcPCC.getTotalDistance();
            }

            if (leastDistance == 0) {
                leastDistance = tempDistance;
                leastDistanceSegment = thisSegment;
            } else {
                if(tempDistance<leastDistance){
                    leastDistance = tempDistance;
                    leastDistanceSegment = thisSegment;
                }
            }
        }

        if(Info.length > 5) // if true, shapelet is feature-diff base, not distance base.
            leastDistance = ExtractStatisticFeatures.getOneFeatureValue(shapeletData.get(ShapeletIndex), Info[5]) - ExtractStatisticFeatures.getOneFeatureValue(leastDistanceSegment, Info[5]);

        return leastDistance;
    }
}
