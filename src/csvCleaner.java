import Features.Madgwick;
import Features.VelocityEstimation;
import Utils.FileWorker;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by nyotie on 16-May-17.
 */
public class csvCleaner {
    private static String dataPath = "";
    private static String labelValue = "";

    private static boolean addQuaternion = false;
    private static boolean addVelocity = false;

    private static boolean addLabel = false;
    private static boolean changeLabel = false; // always false if addLabel=true, if no then depend on setting
    private static int labelColumnPosition = 15; // index start from 0

    private static ArrayList<Integer> colPosToDelete = new ArrayList<>(Arrays.asList(0,1,16)); //uncomment to add column positions

    private static int dataRate = 50;
    private static Madgwick madgwickQuat = new Madgwick((float) 1/dataRate);
    private static VelocityEstimation EstVelocity = new VelocityEstimation((float) 1/dataRate);

    public static void main(String[] args) throws IOException {

        // absolute rules
        changeLabel = !addLabel && changeLabel;
        addVelocity = addQuaternion && addVelocity;

        // get list of files
        ArrayList<String> listOfFiles = new ArrayList<>();
        try(Stream<Path> paths = Files.walk(Paths.get(dataPath)).filter(Files::isRegularFile)) {
            paths.forEach(filePath -> listOfFiles.add(filePath.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // create var in case needed
        float[] acc_val = new float[3];
        float[] gyr_val = new float[3];

        // compute for each of file
        int fileCount = 0;
        for (String thisFilePath: listOfFiles){
            String writePath = dataPath + "/temp.csv";
            CSVWriter csvWriter = new CSVWriter(new FileWriter(writePath, new File(writePath).exists()), ',', CSVWriter.NO_QUOTE_CHARACTER);

            File rawSource = new File(thisFilePath);
            CSVReader reader = new CSVReader(new FileReader(rawSource)); // read template csv and store to variable
            String[] nextLine;

            boolean firstRow = true;
            while ((nextLine = reader.readNext()) != null) {
                String thisLineValue="";

                for (int i=0; i<=nextLine.length-1; i++){

                    if(firstRow){ // HEADER
                        if(!contain(colPosToDelete, i)){
                            thisLineValue += nextLine[i] + ((i!=nextLine.length-1) ? "," : "");
                        }
                    } else { // VALUES

                        if(addQuaternion || addVelocity) {
                            switch (i){
                                case 0: acc_val[0] = Float.valueOf(nextLine[i]);
                                case 1: acc_val[1] = Float.valueOf(nextLine[i]);
                                case 2: acc_val[2] = Float.valueOf(nextLine[i]);
                                case 3: gyr_val[0] = Float.valueOf(nextLine[i]);
                                case 4: gyr_val[1] = Float.valueOf(nextLine[i]);
                                case 5: gyr_val[2] = Float.valueOf(nextLine[i]);
                            }
                        }

                        if(contain(colPosToDelete, i)){
                            if(i==nextLine.length-1){ // means last column
                                if(Objects.equals(thisLineValue.substring(thisLineValue.length() - 1), ",")) // if last string is a comma
                                    thisLineValue = thisLineValue.substring(0, thisLineValue.length()-1); // remove the last comma given
                            }
                        } else {
                            thisLineValue += ((changeLabel && i == labelColumnPosition) ? labelValue : nextLine[i]);
                            thisLineValue += (i!=nextLine.length-1) ? "," : "";
                        }
                    }
                }

                // add quaternion / velocity AFTER finish read all column in a line. the new column will always located at the back end of the column
                if(addQuaternion) {
                    if(firstRow){
                        thisLineValue += ",quat0,quat1,quat2,quat3";
                        if (addVelocity) thisLineValue += ",veloX,veloY,veloZ";
                    } else {
                        float[] quat_val = madgwickQuat.updateQuat(gyr_val, acc_val);
                        thisLineValue += "," + quat_val[0] + "," + quat_val[1] + "," + quat_val[2] + "," + quat_val[3];

                        if (addVelocity) {
                            float[] velo_val = EstVelocity.push(acc_val, quat_val);
                            thisLineValue += "," + velo_val[0] + "," + velo_val[1] + "," + velo_val[2];
                        }
                    }
                }

                // label position is always at last column
                if(addLabel) thisLineValue += "," + ((firstRow) ? "flag" : labelValue);

                // write values to csv
                csvWriter.writeNext(thisLineValue.split(","));

                if(firstRow) firstRow = false;
            }
            csvWriter.close();
            reader.close();

            //delete and rename
            File newFile = new File(writePath);
            FileWorker.RenameFile(rawSource, newFile);

            fileCount++;
        }

        System.out.println("File processed : " + fileCount);
    }

    private static boolean contain(ArrayList<Integer> collection, int now){
        for (int num: collection) {
            if (num == now) {
                return true;
            }
        }
        return false;
    }
}
