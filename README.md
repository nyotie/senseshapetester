### What is this repository for? ###

* Quick summary:  
Third and last App of the batch. This app work in sequence with 2 other apps : Android app to collect data, and SenseShape to build the Decision Tree Model and extract Shapelets. This app test the model and shapelet, to  classify unlabelled data (the data still need to have label tho, for ground truth)  
Android apps to collect data: [Koala Simple Apps](https://bitbucket.org/nyotie/koalasimpleapps)  
SenseShape to build DT Model + Extract the Shapelet: [SenseShape](https://bitbucket.org/nyotie/senseshape)  
SenseShapeTester to test the DT Model and Shapelet: [SenseShapeTester](https://bitbucket.org/nyotie/senseshapetester)

* Version: -

### How do I get set up? ###

* Summary of set up: basically there are 3 classes for you to check  
  1. **ParseTreeToIfElse**: To parse weka decision tree model output into if-else tree code format.  
  2. **ShapeletClassifier**: The class where you put the if-else tree.  
  3. **RecognizeActivity**: To classify testing dataset and check the precission, recall, and accuracy.  

* Configuration, things to bet set before running:  
**ParseTreeToIfElse**: Change only these variables:  
```String ModelTree = "./data/tree_feature_30.txt";```  
**ShapeletClassifier**: Paste the if-else codes from **ParseTreeToIfElse** to *Classify* function, between these lines:  
```
public String Classify(){
String result;
// start of Classification Tree Model
**paste them here!**
// end of Classification Tree Model
return result;
}
```  
**RecognizeActivity**: Change only these variables:   
```
// Config: Shapelet Path
static String shapeletPath = "./data/shape_feature_30.csv";
// Config: Similarity Calculation
static int distanceMeasurement = 1; // 0=DTW, 1=ED, 2=PCC
// Config: Smoothing Filter
private static boolean filterFirst = true;
static int filterMethod = 1; // 0=Mean, 1=Median, 2=LPF
static int filterWindowSize = 10;
```

* Dependencies:
	1. **SenseShape**, this project uses some codes from previous SenseShape project.
	3. Java version : 1.8

* Path configuration
* How to run tests
* Deployment instructions
	

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Anything, feel free to mail  
Nyoto / garmseven@gmail.com  
Daniel (吳政瑋) / silvemoonfox@hotmail.com

* Other community or team contact